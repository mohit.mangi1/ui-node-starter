/* eslint-disable no-param-reassign */
const csvtojson = require("csvtojson");
const { fileURLToPath } = require("url");
const { dirname } = require("path");
const { readHTMLTemplate } = require("../common/utils.js");
const { REQ_FEATURE_KEY } = require("../middlewares/abtest/constants/index.js");

const isProduction = ["production", "qa", "dev"].includes(process.env.NODE_ENV);
// const memoizeHtml = memoize(readHTMLTemplate);
module.exports.serveHTMLTemplate = (
  req,
  res,
  { filePath, uiConfig, basePath }
) => {
  console.log("Request for landing page");
  if (isProduction) {
    const htmlResponse = readHTMLTemplate(
      filePath,
      {
        ...uiConfig,
        abConfig: req[REQ_FEATURE_KEY],
      },
      basePath
    );
    res.setHeader("Content-Type", "text/html");
    res.end(htmlResponse);
  } else {
    const htmlResponse = readHTMLTemplate(
      filePath,
      {
        ...uiConfig,
        abConfig: req[REQ_FEATURE_KEY],
      },
      basePath
    );
    res.setHeader("Content-Type", "text/html");
    res.end(htmlResponse);
  }
};
module.exports.readCSV = async (filePath, { lookupCSVKey, lookupCSVValue }) => {
  const csvFilePath = filePath;
  try {
    const response = await csvtojson().fromFile(csvFilePath);
    const responseToMap = response.reduce((currentValue, nextValue) => {
      currentValue[nextValue[lookupCSVKey]] = nextValue[lookupCSVValue];
      return currentValue;
    }, {});
    return Promise.resolve(responseToMap);
  } catch (e) {
    return Promise.reject(e);
  }
};
module.exports.getCSVPath = (url, path) => {
  const __filename = fileURLToPath(url);
  const __dirname = dirname(__filename);
  return `${__dirname}/${path}`;
};
