const { HTTPStatusError } = require("@freighttiger/rest/lib/http");
const { curryN } = require("ramda");
const { resolve } = require("path");
const fs = require("fs");
const handlebars = require("handlebars");
const _ = require("lodash");
const ApiError = require("../error/apierror");
const JsError = require("../error/jserror");
const RequestFailedError = require("../error/requestfailed");

module.exports.getPath = curryN(3, resolve)(__dirname, "../../");
module.exports.dirName = process.cwd();
module.exports.memoize = (fn) => {
  const cache = {};
  return (...params) => {
    const key = JSON.stringify(params);
    if (!cache[key]) {
      cache[key] = fn.apply(this, params);
    }
    return cache[key];
  };
};

module.exports.readHTMLTemplate = (
  filePath,
  config,
  basePath,
  templateConfig
) => {
  const files = fs.readFileSync(filePath).toString("utf8");
  const template = handlebars.compile(files);
  const htmlResponse = template({
    uiConfig: JSON.stringify(config),
    BASE_PATH: basePath,
    templateConfig,
  });
  return htmlResponse;
};
module.exports.isStaticAssetRequest = (req) => {
  if (
    req.originalUrl.indexOf(".js") >= 0 ||
    req.originalUrl.indexOf(".css") >= 0 ||
    req.originalUrl.indexOf("/img/") >= 0 ||
    req.originalUrl.indexOf("/applogo/") >= 0 ||
    req.originalUrl.indexOf("/fonts/") >= 0 ||
    req.originalUrl.indexOf(".ico") >= 0
  ) {
    return true;
  }
  return false;
};
module.exports.handleAxiosError = (error) => {
  console.log("failed with error code", error.code);
  console.log("error description");
  console.log(error);
  if (error instanceof ApiError) {
    // Request made and server responded
    throw new HTTPStatusError(error.status, {
      message:
        error && error.data && error.data && error.data.value
          ? error.data.value
          : error.data,
    });
  } else if (error instanceof RequestFailedError) {
    // The request was made but no response was received
    throw new HTTPStatusError(error.status, {
      message: "Error while processing the request",
    });
  } else if (error instanceof JsError) {
    // Something happened in setting up the request that triggered an Error
    throw new HTTPStatusError(400, {
      message: "Something went wrong!",
    });
  } else {
    throw new HTTPStatusError(400, {
      message: "Something went wrong!",
    });
  }
};
module.exports.removeQueryParams = (originalUrl) => originalUrl.split("?")[0];
const padZeros = (mask, length) => {
  const paddingSize = length - mask.length;
  const paddingArray = Array.from(Array(paddingSize), () => 0);
  const paddedArray = mask.concat(paddingArray);
  return paddedArray;
};

module.exports.isAuthorized = (
  userPermissions,
  permissions = [],
  allPermissions
) => {
  const finalPermissions = {};
  permissions.forEach((permission) => {
    const type =
      allPermissions[permission] && allPermissions[permission].permission_type;
    let mask1 = [];
    let mask2 = [];
    let isset = false;

    switch (type) {
      case "CD":
        mask1 = userPermissions.permission_create_mask;
        mask2 = allPermissions[permission].permission_create_mask;
        break;
      case "U":
        mask1 = userPermissions.permission_update_mask;
        mask2 = allPermissions[permission].permission_update_mask;
        break;
      case "R":
        mask1 = userPermissions.permission_read_mask;
        mask2 = allPermissions[permission].permission_read_mask;
        break;
      default:
        break;
    }
    if (mask1.length > mask2.length) {
      mask2 = padZeros(mask2, mask1.length);
    } else if (mask2.length > mask1.length) {
      mask1 = padZeros(mask1, mask2.length);
      console.log(mask1, mask2);
    }
    console.log(mask1, mask2);
    mask1.forEach((value, index) => {
      // eslint-disable-next-line no-bitwise
      if (value & mask2[index]) {
        isset = true;
      }
    });
    finalPermissions[permission] = isset;
  });
  const isDisplay = _.values(finalPermissions).every((item) => item === true);
  return isDisplay;
};
