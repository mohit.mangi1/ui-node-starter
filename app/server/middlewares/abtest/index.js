const testAbTest = require('./feature/testab/user/index.js');
const {
  TEST_AB_FEATURE,
  REQ_FEATURE_KEY,
} = require('./constants/index.js');
// const earningTabFeatureConfig = earning({
//   lookupCSVKey: "demandid",
//   lookupCSVValue: "value",
//   reqFind: "headers",
//   lookupType: "map",
//   reqKey: "demandid",
// });
const testAbFeature = testAbTest();
const activeAbTest = {
  [TEST_AB_FEATURE]: testAbFeature,
};
module.exports = (req, res, next) => {
  if (!req.static_asset) {
    req[REQ_FEATURE_KEY] = {};
    const keys = Object.keys(activeAbTest);
    keys.forEach((feature) => {
      const {
        featureName,
        reqKey,
        lookupMap,
        lookupType,
        reqFind,
        executor,
        laterExecutor,
        whitelistURL,
      } = activeAbTest[feature];
      console.log(feature);
      if (
        featureName === feature
        && (whitelistURL.includes(req.originalUrl) || whitelistURL.includes('*'))
      ) {
        switch (lookupType) {
          case 'map': {
            if (lookupMap[req[reqFind][reqKey]]) {
              console.log('here', req[reqFind][reqKey]);
              req[REQ_FEATURE_KEY][featureName] = true;
            }
            break;
          }
          case 'list': {
            if (lookupMap.includes(req[reqFind][reqKey])) {
              req[REQ_FEATURE_KEY][featureName] = true;
            }
            break;
          }
          case 'random': {
            executor(req, res, (value) => {
              if (value) {
                laterExecutor(req, res, () => {
                  req[REQ_FEATURE_KEY][featureName] = true;
                });
              } else {
                req[REQ_FEATURE_KEY][featureName] = false;
              }
            });
            break;
          }
          default: {
            if (lookupMap[req[reqFind][reqKey]]) {
              console.log('here', req[reqFind][reqKey]);
              req[REQ_FEATURE_KEY][featureName] = true;
            }
            break;
          }
        }
      }
    });
  }
  next();
};
