const { readCSV } = require("../../../../../filehelper/index.js");
const { TEST_AB_FEATURE } = require("../../../constants/index.js");

module.exports = async ({
  lookupCSVKey = "email",
  lookupCSVValue = "value",
  reqFind = "headers",
  lookupType = "map",
  reqKey = "email",
} = {}) => {
  try {
    const response = await readCSV(`${__dirname}/email.csv`, {
      lookupCSVKey,
      lookupCSVValue,
    });

    return {
      featureName: TEST_AB_FEATURE,
      lookup: lookupCSVKey,
      value: lookupCSVValue,
      reqKey,
      lookupType,
      lookupMap: response,
      reqFind,
      whitelistURL: ["*"],
    };
  } catch (e) {
    console.log(e);
    return e;
  }
};
