const ab = require("express-ab");
const { TEST_AB_FEATURE } = require("../../../constants/index.js");

const pageTest = ab.test(TEST_AB_FEATURE);
module.exports = function (distribution, distribution2) {
  const exector = pageTest(null, distribution);
  const executor2 = pageTest(null, distribution2);
  return {
    lookupType: "random",
    featureName: TEST_AB_FEATURE,
    executor: exector,
    laterExecutor: executor2,
    whitelistURL: ["/"],
  };
};
