const express = require("express");

const expiry = 31536000;
const path = require("path");
const resolve = (file) => path.resolve(__dirname, file);
module.exports = express.static(resolve("../../../dist/"), {
  maxage: expiry,
  etag: false,
  index: false,
  setHeaders: function (res) {
    res.header("Pragme", null);
    res.setHeader("Cache-Control", "public, max-age=31557600");
    res.setHeader("Expires", new Date(Date.now() + 31536000).toUTCString());
  },
});
