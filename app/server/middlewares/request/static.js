const { isStaticAssetRequest } = require("../../common/utils.js");

module.exports = (req, res, next) => {
  console.log(req.originalUrl);
  if (isStaticAssetRequest(req)) {
    req.static_asset = true;
  }
  next();
};
