const express = require("express");
const router = express.Router();
const { removeQueryParams } = require("../common/utils.js");
const whitelistedurl = require("./mock/whitelistedurl");
router.use((req, res, next) => {
  res.setHeader("Surrogate-Control", "no-store");
  res.setHeader(
    "Cache-Control",
    "no-store, no-cache, must-revalidate, proxy-revalidate"
  );
  res.setHeader("Pragma", "no-cache");
  res.setHeader("Expires", "0");
  next();
});
router.use("/*", (req, res, next) => {
  if (process.env.MOCK_ENABLED === "true") {
    let url = removeQueryParams(req.originalUrl);
    if (whitelistedurl.includes(url)) {
      let rawdata = require(`./mock${url}.json`);
      const { success, status, data } = rawdata;
      return res.status(rawdata.status).json({ success, status, data });
    } else {
      console.log(`not a whitelisted mock url ${url}`);
    }
  }
  return next();
});
router.use("/app1", require("./app1/app1-router").Router);
module.exports = router;
