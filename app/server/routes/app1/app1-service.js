const errorHandler = require("../../error");
const config = require("@freighttiger/app").ConfigAccessor.getConfig();
const testServiceRequest = require("axios").create({
  baseURL: config.service && config.service.test.base_url,
  timeout: config.service && config.service.test.timeout,
});

testServiceRequest.interceptors.response.use(
  (response) => {
    if (response.data) {
      if (response.data.data) return response.data.data;
      return response.data;
    } else {
      return errorHandler({ message: "Object not found" });
    }
  },
  (err) => {
    return errorHandler(err);
  }
);
class TestService {
  constructor() {
    console.log("initializing Test service");
  }
}

module.exports = new TestService();
