const { GET } = require("@freighttiger/rest/lib/http").HTTPMethod.Methods;
const { BasicRouter } = require("@freighttiger/rest");
const RouteController = require("./app1-controller");

class TestRouter extends BasicRouter {
  _getController() {
    return RouteController;
  }

  async _routing() {
    this._route("/abconfig", GET, await this._controller._getAbConfig());
  }
}
module.exports = new TestRouter();
