const { BasicController } = require("@freighttiger/rest");
const abConfigController = require("./controller/ab-controller");
class TestController extends BasicController {
  constructor() {
    super();
  }
  async _getAbConfig() {
    return abConfigController;
  }
}
module.exports = new TestController();
