const { SuccessOutput } = require("@freighttiger/rest/lib/response/output");
const { handleAxiosError } = require("../../../common/utils");
const { REQ_FEATURE_KEY } = require("../../../middlewares/abtest/constants");
module.exports = async function (req) {
  try {
    const abFeatures = {
      enableFeature: req[REQ_FEATURE_KEY],
    };
    return new SuccessOutput(abFeatures, 200);
  } catch (error) {
    return handleAxiosError(error);
  }
};
