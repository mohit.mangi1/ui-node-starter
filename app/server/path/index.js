const { getPath } = require("../common/utils");
const appName = "app1";
module.exports = {
  clientEntryFile: getPath(`./ui/${appName}/src/entry-client.js`),
  serverEntryFile: getPath(`./ui/${appName}/src/entry-server.js`),
  clientNewrelicFile: getPath(`./ui/${appName}/src/newrelic.js`),
  regisetAppApiPath: `/${appName}`,
  appModulePath: {
    [appName]: getPath(`./server/ssr/${appName}`), //make sure the module is available in the ssr directory of server app
  },
};
