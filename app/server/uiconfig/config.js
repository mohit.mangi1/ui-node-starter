module.exports = {
    APP1_UI_CONFIG: {
      GatewayURL: process.env.GATEWAY_URL,
      NEW_RELIC_UI_ACCOUNT_ID: process.env.NEW_RELIC_UI_ACCOUNT_ID,
      NEW_RELIC_UI_TRUST_KEY: process.env.NEW_RELIC_UI_TRUST_KEY,
      NEW_RELIC_UI_AGENT_ID: process.env.NEW_RELIC_UI_AGENT_ID,
      NEW_RELIC_UI_APPLICATION_ID: process.env.NEW_RELIC_UI_APPLICATION_ID,
      NEW_RELIC_UI_LICENSE_KEY: process.env.NEW_RELIC_UI_LICENSE_KEY,
      basePathRouter: "/",
    }
  };
  