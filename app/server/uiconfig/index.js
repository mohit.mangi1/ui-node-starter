const config = require("./config.js");
module.exports = process.env.NODE_ENV === "production" ? config : config;
