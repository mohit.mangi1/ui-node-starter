/* eslint-disable no-param-reassign */
const express = require("express");
const Compression = require("compression");
const CookieParser = require("cookie-parser");
const fs = require("fs");
const spdy = require("spdy");
const { BasicApplication } = require("@freighttiger/app");
const { dirName } = require("../common/utils");

class InitialiseDevServer extends BasicApplication {
  constructor() {
    super(`${dirName}/server/conf`);
  }

  async _initDependencies() {
    const SubApps = require("../../subapp");
    const subApps = new SubApps();
    this._mainApp = express();
    subApps.init(this._mainApp);
    this._subApp = subApps.apps;
    this.start(this._config);
  }

  start(config) {
    const { _mainApp: app } = this;
    app.use(express.json());
    app.use(Compression());
    app.use(express.urlencoded({ extended: true }));
    app.use(CookieParser());
    const spdyoptions = {
      key: fs.readFileSync(`${dirName}/server/cert/localhoststage.ft.com.key`),
      cert: fs.readFileSync(`${dirName}/server/cert/localhoststage.ft.com.crt`),
    };
    if (this._subApp && this._subApp.length) {
      this._subApp.forEach((subApp) => {
        subApp = subApp || {};

        if (!subApp.mountPath || !subApp.app) {
          this._logger.error(
            `Can not mount sub-app on path ${subApp.mountPath}`
          );
          return;
        }

        app.use(subApp.mountPath, subApp.app);
      });
    }
    const server = spdy.createServer(spdyoptions, app);
    server.listen(config.server.port, (error) => {
      if (error) {
        // eslint-disable-next-line no-process-exit
        return process.exit(1);
      }
      return console.log(`Listening on port: ${config.server.port}.`);
    });
  }
}
module.exports = InitialiseDevServer;
