const ExtractTextPlugin = require("extract-text-webpack-plugin");
const { concat } = require("ramda");
const { getPath } = require("../../common/utils");

const isProd = process.env.NODE_ENV === "production";

const commonPlugins = [];
const concatCommon = concat(commonPlugins);

const prodPlugins = concatCommon([
  new ExtractTextPlugin({
    filename: "common.[hash].css",
  }),
]);
const devPlugins = concatCommon([]);
const BaseConfiguration = {
  devtool: isProd ? false : "inline-source-map",
  mode: isProd ? "production" : "development",
  output: {
    path: getPath("./dist"),
    chunkFilename: "[name].[hash].bundle.js",
    filename: "[name].[hash].js",
    publicPath: "/",
  },
  resolve: {
    alias: {
      public: getPath("public"),
      vue$: "vue/dist/vue.esm.js",
    },
    extensions: [".js", ".vue", ".json"],
    plugins: [],
  },
  plugins: isProd ? prodPlugins : devPlugins,
  performance: {
    maxEntrypointSize: 300000,
    hints: isProd ? "warning" : false,
  },
  module: {
    unknownContextCritical: false,
    // noParse: /es6-promise/,
    rules: [
      {
        test: /\.vue$/,
        loader: "vue-loader",
        options: {
          extractCSS: isProd,
          loaders: {
            scss: "vue-style-loader!css-loader!sass-loader",
            sass: [
              "vue-style-loader",
              "css-loader",
              {
                loader: "sass-loader",
                options: {
                  indentedSyntax: true,
                },
              },
              {
                loader: "sass-resources-loader",
                options: {
                  resources: [
                    getPath("src/sass/_vars.sass"),
                    getPath("src/sass/_func.sass"),
                  ],
                },
              },
            ],
            styl: "vue-style-loader!css-loader!stylus-loader",
          },
        },
      },
      { test: /\.styl$/, loader: "vue-style-loader!css-loader!stylus-loader" },
      { test: /\.css$/, use: ["style-loader", "css-loader"] },
      {
        test: /\.sass$/,
        loader: `vue-style-loader!css-loader!sass-loader!sass-resources-loader?resources=${getPath(
          "src/sass/_vars.sass"
        )}&resources=${getPath("src/sass/_func.sass")}`,
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{ loader: "babel-loader" }],
      },
      {
        test: /\.(png|jpg|gif|svg|ttf|woff|eot)$/,
        loader: "file-loader",
        options: { name: "[name].[ext]?[hash]" },
      },
    ],
  },
};

module.exports = BaseConfiguration;
