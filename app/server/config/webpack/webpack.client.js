const merge = require("webpack-merge");
const VueSSRClientPlugin = require("vue-server-renderer/client-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const { DefinePlugin } = require("webpack");
const BaseConfiguration = require("./webpack.base");
const { clientEntryFile, clientNewrelicFile } = require("../../path");

const isProd = process.env.NODE_ENV === "production";
// const vendorPackage = ["vuex.vendor", "vue-router.vendor","vue.vendor","core-js.vendor"];
const clientConfiguration = merge(BaseConfiguration, {
  entry: {
    app: clientEntryFile,
    newrelic : clientNewrelicFile
  },
  resolve: {
    extensions: [".js", ".vue", ".json", ".styl", ".css", ".sass", ".scss"],
    alias: {
      vue$: "vue/dist/vue.esm.js",
    },
  },
  plugins: [
    // new DllReferencePlugin({
    //   context: getPath(''),
    //   manifest: 'manifest.json'
    // }),
    new DefinePlugin({
      isClient: true,
      isServer: false,
    }),
    new VueSSRClientPlugin(),
  ],
});

if (isProd) {
  clientConfiguration.plugins.push(new BundleAnalyzerPlugin());
  clientConfiguration.optimization = {
    runtimeChunk: "single", // enable "runtime" chunk
    splitChunks: {
      chunks: 'all',
      maxInitialRequests: Infinity,
      minSize: 0,
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName
            try {
              const packageName = module.context.match(
                /[\\/]node_modules[\\/](.*?)([\\/]|$)/
              )[1];
              const bundleName = `${packageName.replace("@", "")}.vendor`;
              if (vendorPackage.includes(bundleName)) {
                console.log(bundleName)
                return bundleName;
              }
              return "vendor";
            } catch (e) {
              return "vendor";
            }
          },
          chunks: "all",
        },
      },
    },
  };
}

module.exports = clientConfiguration;
