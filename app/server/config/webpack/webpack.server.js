const webpack = require("webpack");
const merge = require("webpack-merge");
const nodeExternals = require("webpack-node-externals");
const VueSSRServerPlugin = require("vue-server-renderer/server-plugin");
const BaseConfiguration = require("./webpack.base");
const { serverEntryFile } = require("../../path");

module.exports = merge(BaseConfiguration, {
  target: "node",
  devtool: "source-map",
  entry:serverEntryFile,
  output: {
    filename: "server-bundle.js",
    libraryTarget: "commonjs2",
  },
  resolve: {
    extensions: [".js", ".vue", ".json", ".styl", ".css", ".sass", ".scss"],
  },
  externals: [
    nodeExternals({
      whitelist: [/\.css$/, /\.styl$/, /\.vue$/],
    }),
  ],
  plugins: [
    new webpack.DefinePlugin({
      isServer: true,
      isClient: false,
    }),
    new VueSSRServerPlugin(),
  ],
});
