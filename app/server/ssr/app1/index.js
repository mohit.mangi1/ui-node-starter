const serviceConfig = require("@freighttiger/app").ConfigAccessor;
const { createBundleRenderer } = require("vue-server-renderer");
const path = require("path");
const fs = require("fs");
const LRU = require("lru-cache");
const waitParcel = require("../../config/index");
const isProduction = ["production", "qa", "dev"].includes(process.env.NODE_ENV);
const resolve = (file) => path.resolve(__dirname, file);
const { getPath } = require("../../common/utils");
const cache = new LRU({
  max: 1000,
  maxAge: 1000 * 60 * 15,
});
function createRenderer(bundle, options) {
  console.log(resolve("../../dist"));
  return createBundleRenderer(
    bundle,
    Object.assign(options, {
      cache,
      basedir: resolve("../../dist"),
      runInNewContext: false,
    })
  );
}
const handleError = (err, req, res) => {
  if (err.url) {
    res.redirect(err.url);
  } else if (err.code === 404) {
    res.status(404).send(err);
  } else {
    console.log(err);
    res.end("Internal server error");
    console.error(`error during render : ${req.url}`);
    console.error(err.stack);
  }
};
class App1Module {
  render(req, res) {
    const s = Date.now();
    res.setHeader("Content-Type", "text/html");
    const config = serviceConfig.getConfig();
    console.log(config);
    const context = {
      title: "App",
      url: req.params.id,
      BASE_PATH: config.ui_config.router_base_path,
      uiConfig: JSON.stringify(config.ui_config.app1),
    };
    this.renderer.renderToString(context, (err, html) => {
      if (err) {
        console.log(err);
        return handleError(err, req, res);
      }
      if (!isProduction) {
        console.log(`whole request: ${Date.now() - s}ms`);
      }
      return res.send(html);
    });
  }
  install(app, appName) {
    this.appName = appName;
    const templatePath = getPath(`./ui/${appName}/index-ssr.html`);
    if (isProduction) {
      const template = fs.readFileSync(templatePath, "utf-8");
      const bundle = require(getPath(`./dist/vue-ssr-server-bundle.json`));
      const clientManifest = require(getPath(
        "./dist/vue-ssr-client-manifest.json"
      ));
      this.renderer = createRenderer(bundle, {
        template,
        clientManifest,
      });
    } else {
      this.readyPromise = waitParcel(app, templatePath, (bundle, options) => {
        this.renderer = createRenderer(bundle, options);
      });
    }
  }

  serveHtml = (req, res) => {
    if (isProduction) {
      return this.render(req, res);
    }
    return this.readyPromise.then(() => this.render(req, res));
  };
}
module.exports = new App1Module();
