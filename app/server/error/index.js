const ApiError = require("./apierror");
const JsError = require("./jserror");
const RequestFailedError = require("./requestfailed");

module.exports = function (error) {
  if (error.response) {
    // Request made and server responded
    const { status, data } = error.response;
    throw new ApiError("API_REQ_FAILED", { data, status }, "FT_APP_ERR_100");
  } else if (error.request) {
    console.log("erro", error.code);
    // The request was made but no response was received
    if (error.code === "ECONNABORTED") {
      throw new RequestFailedError(
        "Something went wrong",
        408,
        "FT_APP_ERR_105"
      );
    } else {
      // The request was made but no response was received
      throw new RequestFailedError(
        "Something went wrong",
        400,
        "FT_APP_ERR_101"
      );
    }
  } else if (error.message) {
    // Something happened in setting up the request that triggered an Error
    throw new JsError(error.message, 400, "FT_APP_ERR_103");
  } else {
    console.log("FT_APP_ERR_104");
    console.log(error);
    throw new Error("Something went wrong!");
  }
};
