import Vue from "vue";
import { StoreModule } from "./module/store";
import { RouterModule } from "./module/router";
import { CoreModule } from "./module/core";
import { HomeModule } from "./module/home";
import axios from "axios";
function initializeApp(context) {
  //initialize store and router here
  const storeModule = new StoreModule();
  storeModule.install(Vue);
  console.log(context, "context from app");
  const routerModule = new RouterModule();
  routerModule.install(Vue, context.uiConfig.router_base_path);
  const store = storeModule.store;
  const router = routerModule.router;
  //intitialize core app module which will host subapps
  const coreModule = new CoreModule({
    store,
    router,
  });
  coreModule.install(Vue, "#app");
  // const httpModule = new HttpModule(); // create http module

  // initialize page wise module here
  const httpRequest = axios; // get http request instance
  const homeModule = new HomeModule(
    {
      store,
      router,
      httpRequest,
    },
    {}
  );
  homeModule.install();
  if (homeModule.interceptors) {
    const { request: requestInterceptor, response: responseInterceptor } =
    homeModule.interceptors || {};
    responseInterceptor.forEach((interceptors)=>{
      console.log(interceptors)
      axios.interceptors.response.use(interceptors[0],interceptors[1])
    })
  }
  console.log("app is initialized");
  return {
    app: coreModule.app,
    router: routerModule.router,
    store: storeModule.store,
  };
}
export default initializeApp;
