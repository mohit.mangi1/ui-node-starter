import HttpRequest from "./helper/http/httpRequest";
export class HttpModule {
  constructor() {
    this.httpRequest = new HttpRequest();
    this.serviceMap = new Map();
  }
  setDefaultErrorFunction(interceptor) {
    if (interceptor) {
      if (typeof interceptor != "function") {
        interceptor = function (error) {
          return Promise.reject(error);
        };
      }
    } else {
      interceptor = function (error) {
        return Promise.reject(error);
      };
    }
    return interceptor;
  }
  addInterceptor({ interceptors }) {
    if (interceptors) {
      const responseInterceptor = interceptors.response || [];
      const requestInterceptor = interceptors.request || [];
      if (responseInterceptor.length) {
        responseInterceptor.forEach((interceptor) => {
          if (interceptor.length) {
            interceptor[1] = this.setDefaultErrorFunction(interceptor[1]);
            if (typeof interceptor[0] === "function") {
              this.httpRequest.interceptors.response.use(
                interceptor[0],
                interceptor[1]
              );
            } else {
              console.log("unable to register the interceptor");
            }
          }
        });
      }
      if (requestInterceptor.length) {
        requestInterceptor.forEach((interceptor) => {
          if (interceptor.length) {
            interceptor[1] = this.setDefaultErrorFunction(interceptor[1]);
            if (typeof interceptor[0] === "function") {
              console.log(interceptor);
              this.httpRequest.interceptors.request.use(
                interceptor[0],
                interceptor[1]
              );
            } else {
              console.log("unable to register the interceptor");
            }
          }
        });
      }
    }
  }
  get instance() {
    if (this.httpRequest) return this.httpRequest.getInstance;
    else throw new Error("Http Module is not initialised");
  }
}
