let HttpResponse = function (xhr) {
  this.body = xhr.response;
  this.status = xhr.status;
  this.headers = xhr
    .getAllResponseHeaders()
    .split("\r\n")
    .reduce((result, current) => {
      let [name, value] = current.split(": ");
      result[name] = value;
      return result;
    }, {});
  this.parser = new DOMParser();
};

HttpResponse.prototype.json = function () {
  return JSON.parse(this.body);
};

HttpResponse.prototype.getAsDOM = function () {
  return this.parser.parseFromString(this.body, "text/html");
};

export default HttpResponse;
