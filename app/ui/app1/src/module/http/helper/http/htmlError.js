let HttpError = function (xhr) {
  this.body = xhr.response;
  this.status = xhr.status;
  this.headers = xhr
    .getAllResponseHeaders()
    .split("\r\n")
    .reduce((result, current) => {
      let [name, value] = current.split(": ");
      result[name] = value;
      return result;
    }, {});
};

HttpError.prototype.toString = function () {
  try {
    let json = JSON.parse(this.body);
    return {
      json: json,
      status: this.status,
      headers: this.headers,
    };
  } catch (e) {
    return {
      status: this.status,
      headers: this.headers,
      data: this.body,
    };
  }
};
export default HttpError;
