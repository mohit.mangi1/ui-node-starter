const { default: HttpError } = require("./htmlError");
const { default: HttpResponse } = require("./htmlResponse");
const successRegex = [200, 201, 203, 204, 205, 206];
function InterceptorManager() {
  this.handlers = [];
}
InterceptorManager.prototype.use = function use(fulfilled, rejected, options) {
  console.log("Registered interceptor");
  this.handlers.push({
    fulfilled: fulfilled,
    rejected: rejected,
    synchronous: options ? options.synchronous : false,
    runWhen: options ? options.runWhen : null,
  });
  return this.handlers.length - 1;
};
InterceptorManager.prototype.forEach = function forEach(fn) {
  this.handlers.forEach(function (h) {
    if (h !== null) {
      fn(h);
    }
  });
};
let httpRequest = function () {
  this.interceptors = {
    request: new InterceptorManager(),
    response: new InterceptorManager(),
  };
  this.getInstance = ({ method, url, params={}, data={}, headers={} }) => {
    method = method.toUpperCase();

    let queryString = params
      ? Object.keys(params)
          .map((key) => key + "=" + params[key])
          .join("&")
      : "";
    let xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.open(method, `${url}?${queryString}`);
    xhr.setRequestHeader("Content-Type", "application/json");
    for (const key in headers) {
      if (Object.hasOwnProperty.call(headers, key)) {
        xhr.setRequestHeader(key, headers[key]);
      }
    }

    xhr.send(data);
    var responseInterceptorChain = [];
    this.interceptors.response.forEach(function pushResponseInterceptors(
      interceptor
    ) {
      responseInterceptorChain.push(
        interceptor.fulfilled,
        interceptor.rejected
      );
    });
    let promise = new Promise((resolve, reject) => {
      xhr.onload = () => {
        if (successRegex.includes(xhr.status)) {
          resolve(new HttpResponse(xhr));
        } else {
          reject(new HttpError(xhr).toString());
        }
      };

      xhr.onerror = () => {
        reject(new HttpError(xhr).toString());
      };
    });

    while (responseInterceptorChain.length) {
      promise = promise.then(
        responseInterceptorChain.shift(),
        responseInterceptorChain.shift()
      );
    }
    return promise;
  };
};
export default httpRequest;
