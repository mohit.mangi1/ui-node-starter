//pages
export const viewPageRegistry = {
  About: () => import(/* webpackChunkName: "about" */ "../view/About.vue"),
};

//generic component
export const commonComponentRegistry = {
  Header: () =>
    import(
      /* webpackChunkName: "Header" */ "../../../../../components/header/Header.vue"
    ),
  Button: () =>
    import(
      /* webpackChunkName: "Button" */ "../../../../../components/button/Button.vue"
    ),
  NavBar: () =>
    import(
      /* webpackChunkName: "NavBar" */ "../../../../../components/navbar/NavBar.vue"
    ),
};
