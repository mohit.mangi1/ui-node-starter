class AboutStore {
  aboutService;
  constructor() {}
  initialize(aboutService) {
    this.aboutService = aboutService;
  }
  get module() {
    // let self = this;
    return {
      namespaced: true,
      state: () => {},
      mutations: {},
      actions: {},
      modules: {},
    };
  }
}

export default new AboutStore();
