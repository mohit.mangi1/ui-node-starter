import { viewPageRegistry } from "../registry";
export default [
  {
    path: "/about",
    name: "about",
    component: viewPageRegistry.About,
  },
];
