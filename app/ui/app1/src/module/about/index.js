import aboutStore from "./store";
import aboutRoutes from "./router";
import aboutService from "./service";
import InstallPageModule from "../../interface/installPageModule";
export class AboutModule extends InstallPageModule {
  storeRegisterName = "AboutModule";
  constructor({ store, router, httpRequest }) {
    super(store, router, httpRequest, {
      service: aboutService,
      pageStore: aboutStore,
      pageRoutes: aboutRoutes,
    });
  }

  install() {
    super.install(this.storeRegisterName);
  }
}
