import { viewPageRegistry } from "../registry";
export default [
  {
    path: "/home",
    name: "home",
    component: viewPageRegistry.HomePage,
  },
];
