import homeStore from "./store";
import homeRoutes from "./router";
import homeService from "./service";
import InstallPageModule from "../../interface/installPageModule";

export class HomeModule extends InstallPageModule {
  storeRegisterName = "HomeModule";
  constructor({ store, router, httpRequest }) {
    super(store, router, httpRequest, {
      pageStore: homeStore,
      pageRoutes: homeRoutes,
      service: homeService,
    });
    this.interceptors = { response: [], request: [] };
  }
  getResponseInterceptors = () => {
    return [
      (response) => {
        console.log("intercepting home success response");
        console.log(response);
        return response;
      },
      (error) => {
        console.log("intercepting home error response");
        return Promise.reject(error);
      },
    ];
  };
  install() {
    super.install(this.storeRegisterName);
    this.interceptors["response"] = this.getResponseInterceptors();
  }
}
