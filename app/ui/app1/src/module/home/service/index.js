/* eslint-disable no-undef */
class HomeService {
  constructor(httpRequest) {
    this.httpRequest = httpRequest;
  }
  async callGetData() {
    try {
      const response = await this.httpRequest({
        method: "GET",
        url: "https://jsonplaceholder.typicode.com/todos/1",
        params: {
          hello: true,
          hello2: false,
        },
        data: {},
        headers: {},
      });
      if (isClient) {
        return response.json();
      } else {
        return response.data;
      }
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
export default HomeService;
