class HomeStore {
  homeService;
  constructor() {}
  initialize(homeService) {
    this.homeService = homeService;
  }
  get module() {
    const self = this;
    return {
      namespaced: true,
      state: () => {
        return {
          testData: null,
        };
      },
      mutations: {
        setTestData(state, token) {
          state.testData = token;
        },
      },
      actions: {
        async callGetData({ commit }, data) {
          return self.homeService.callGetData(data).then((value) => {
            commit("setTestData", value);
          });
        },
      },
      modules: {},
    };
  }
}
export default new HomeStore();
