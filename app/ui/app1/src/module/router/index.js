import VueRouter from "vue-router";
export class RouterModule {
  router;
  constructor() {}

  install(Vue, basePath) {
    Vue.use(VueRouter);
    console.log(basePath);
    this.router = new VueRouter({
      mode: "history",
      base: basePath,
      routes: [],
    });
    console.log("router intiatilzed");
  }
}
