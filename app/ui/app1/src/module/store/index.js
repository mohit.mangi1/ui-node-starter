import Vuex from "vuex";
export class StoreModule {
  store;
  constructor() {}

  install(Vue) {
    Vue.use(Vuex);
    this.store = new Vuex.Store({
      state: {},
      mutations: {},
      actions: {},
      modules: {},
    });
  }
}
