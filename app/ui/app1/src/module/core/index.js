import App from "./view/App.vue";
import { sync } from "vuex-router-sync";
export class CoreModule {
  app;
  constructor({ store, router }) {
    this.services = new Map();
    this.store = store;
    this.router = router;
  }
  initializeApp(
    Vue,
    id,
    { route: { syncModuleName = "RouteModule" } = {} } = {}
  ) {
    this.unsync = sync(this.store, this.router, { moduleName: syncModuleName });
    this.app = new Vue({
      router: this.router,
      store: this.store,
      render: (h) => {
        console.log("App mounted");
        return h(App);
      },
    }).$mount(id || "#app");
  }
  install(Vue, id) {
    this.initializeApp(Vue, id);
  }
}
