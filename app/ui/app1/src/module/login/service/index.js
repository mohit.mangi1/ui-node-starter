class LoginService {
  constructor(httpRequest) {
    this.httpRequest = httpRequest;
  }
  async callLogin() {
    try {
      const response = await this.httpRequest({
        method: "GET",
        url: "https://jsonplaceholder.typicode.com/todos/1",
        params: {},
        headers: {},
      });
      return response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
export default LoginService;
