import loginStore from "./store";
import loginRoutes from "./router";
import loginService from "./service";
import InstallPageModule from "../../interface/installPageModule";
export class LoginModule extends InstallPageModule {
  storeRegisterName = "LoginModule";
  constructor({ store, router, httpRequest }, options) {
    super(store, router, httpRequest, {
      service: loginService,
      pageStore: loginStore,
      pageRoutes: loginRoutes,
    });
    this.loginOptions = options;
    this.interceptors = { response: [], request: [] };
  }
  getResponseInterceptors = () => {
    return [
      (response) => {
        console.log("intercepting success response");
        console.log(response);
        return response;
      },
      (error) => {
        if (error.status === 401) {
          this.router.push({
            name: this.loginOptions.loginPageName,
          });
          this.router.store.dispatch(this.loginOptions.logoutAction);
        }
        return Promise.reject(error);
      },
    ];
  };
  install() {
    super.install(this.storeRegisterName);
    this.interceptors["response"] = this.getResponseInterceptors();
  }
}
