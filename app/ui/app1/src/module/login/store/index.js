// loginOptions={
//   loginPath: "/",
//   loginRedirect: "/home",
//   currentUrl: location.href,
//   referrer: document.referrer,
// };
class LoginStore {
  loginService;
  constructor(loginOptions) {
    this.loginOptions = loginOptions;
  }
  initialize(LoginService) {
    this.loginService = LoginService;
  }
  get module() {
    let self = this;
    return {
      namespaced: true,
      getters: {},
      state: () => {
        return {
          token: null,
        };
      },
      mutations: {
        SET_AUTH(state, token) {
          state.token = token;
        },
      },
      actions: {
        async login(context, loginCredentials) {
          return await self.loginService.callLogin(loginCredentials);
        },
        async logOut({ commit }) {
          return commit("SET_AUTH", null);
        },
      },
      modules: {},
    };
  }
}

export default new LoginStore();
