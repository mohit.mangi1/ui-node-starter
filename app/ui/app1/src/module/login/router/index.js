import { viewPageRegistry } from "../registry";
export default [
  {
    path: "/login",
    name: "login",
    component: viewPageRegistry.Login,
  },
];
