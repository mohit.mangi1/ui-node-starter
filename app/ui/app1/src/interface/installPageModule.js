export default class InstallPageModule {
  constructor(
    store,
    router,
    httpRequest,
    { pageRoutes = [], pageStore, service }
  ) {
    this.store = store;
    this.router = router;
    this.httpRequest = httpRequest;
    this.pageRoutes = pageRoutes;
    this.pageStore = pageStore;
    this.service = service;
  }
  install(storeRegisterName) {
    this.pageRoutes.forEach((route) => {
      this.router.addRoute(route);
    });
    if (this.pageStore) {
      const service = this.service ? new this.service(this.httpRequest) : null;
      this.pageStore.initialize(service);

      this.store.registerModule([storeRegisterName], this.pageStore.module, {
        preserveState: isClient,
      });
    }
  }
}
