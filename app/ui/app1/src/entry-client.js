import Vue from "vue";
import { StoreModule } from "./module/store";
import { RouterModule } from "./module/router";
import { CoreModule } from "./module/core";
import { pageModules, pageModulesOptions, libraryModules } from "./registry";
async function initializeApp() {
  //initialize store and router here
  const storeModule = new StoreModule();
  storeModule.install(Vue);
  const routerModule = new RouterModule();
  routerModule.install(Vue, window.uiConfig.router_base_path);
  const store = storeModule.store;
  const router = routerModule.router;
  //intitialize core app module which will host subapps
  const coreModule = new CoreModule({
    store,
    router,
  });
  coreModule.install(Vue, "#app");
  if (window.__INITIAL_STATE__) {
    // We initialize the store state with the data injected from the server
    store.replaceState(window.__INITIAL_STATE__);
  }
  //initialize http module
  const httpModuleRef = await libraryModules["HttpModule"](); //load library file
  const httpModule = new httpModuleRef["HttpModule"](); // create http module

  //initialize page wise module here
  const httpRequest = httpModule.instance; // get http request instance
  for (const module in pageModules) {
    //loop through page modules and register module
    const regiserInterceptor = {
      interceptors: {
        response: [],
        request: [],
      },
    };
    const ViewModule = await pageModules[module]();
    const viewModule = new ViewModule[module](
      {
        store,
        router,
        httpRequest,
      },
      pageModulesOptions[module]
    );
    viewModule.install();
    if (viewModule.interceptors) {
      const { request: requestInterceptor, response: responseInterceptor } =
        viewModule.interceptors || {};
      if (requestInterceptor) {
        regiserInterceptor.interceptors.request.push(requestInterceptor);
      }
      if (responseInterceptor) {
        regiserInterceptor.interceptors.response.push(responseInterceptor);
      }
      httpModule.addInterceptor(regiserInterceptor);
    }
  }
  // once all the page modules are installed make sure to register services in http module
}
initializeApp();
