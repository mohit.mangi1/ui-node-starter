export const pageModules = {
  HomeModule: () =>
    import(/* webpackChunkName: "home_module" */ "../module/home"),
  LoginModule: () =>
    import(/* webpackChunkName: "login_module" */ "../module/login"),
  AboutModule: () =>
    import(/* webpackChunkName: "about_module" */ "../module/about"),
};
export const pageModulesOptions = {
  LoginModule: {
    loginPageName: "login",
    loginAction: "HomeModule/redirectHome",
    logoutAction: "LoginModule/logout",
    currentUrl: location.href,
    referrer: document.referrer,
  },
  HomeModule: {},
  AboutModule: {},
};
export const libraryModules = {
  HttpModule: () => import(/* webpackChunkName: "http" */ "../module/http"),
};
