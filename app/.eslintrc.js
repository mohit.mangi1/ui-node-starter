module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ["plugin:vue/essential", "@vue/prettier", "eslint:recommended"],
  parserOptions: {
    parser: "babel-eslint",
  },
  globals: {
    isClient: "on",
    isServer: "on",
  },
  rules: {
    "no-console": "off",
    "import/extensions": "off",
    "no-underscore-dangle": "off",
    "global-require": "off",
    "class-methods-use-this": "off",
  },
};
