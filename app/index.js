require("./env.js");

console.log(process.env.NODE_ENV);
const { Logger } = require("@freighttiger/core");

Logger.initLogger({
  level: "info",
});
const { BasicApplication } = require("@freighttiger/app");
const { dirName } = require("./server/common/utils.js");
const InitialiseDevServer = require("./server/devserve/index.js");

class Application extends BasicApplication {
  constructor() {
    super(`${dirName}/server/conf`);
  }

  async _initDependencies() {
    const SubApps = require("./subapp");
    const subApps = new SubApps();
    subApps.init();
    console.log(subApps.apps);
    this._server = await this._initHTTPServer(this._config.server, {
      subApps: subApps.apps,
    });
  }

  _closeServer() {
    return new Promise((resolve, reject) => {
      this._server.close((error) => {
        if (error) {
          reject(error);
        }
        console.log("Http server closed.");
        resolve("server closed");
      });
    });
  }

  async _closeConnections() {
    console.log("closing the connection");
  }

  async _beforeDestroy() {
    try {
      await this._closeServer();
    } catch (e) {
      console.log(e);
    }
    console.log("before kill");
  }
}
if (["production", "qa", "dev"].includes(process.env.NODE_ENV)) {
  new Application()._start();
} else {
  new InitialiseDevServer()._start();
}
