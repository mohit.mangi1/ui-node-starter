const staticServe = require("./server/middlewares/request/static.js");
const serveAssets = require("./server/middlewares/request/serveassets.js");
const abTest = require("./server/middlewares/abtest/index.js");
const { appModulePath } = require("./server/path/index.js");

class subApps {
  constructor() {
    this.appModulesPath = [];
  }
  init(app) {
    Object.keys(appModulePath).forEach((m) => {
      console.log(appModulePath[m])
      const module = require(appModulePath[m]);
      module.install(app,m);
      const path = {
        mountPath: `/${m}/:id`,
        app: module.serveHtml,
      };
      this.appModulesPath.push(path);
    });
  }
  get apps() {
    return [
      ...this.appModulesPath,
      {
        mountPath: "/*",
        app: staticServe,
      },
      {
        mountPath: "/*",
        app: abTest,
      },
      { mountPath: "/api", app: require("./server/routes") },
      {
        mountPath: "/",
        app: serveAssets,
      },
    ];
  }
}
module.exports = subApps;
