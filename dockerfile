FROM node:14-alpine AS build
WORKDIR /opt/app
COPY ./app/package*.json /opt/app/
RUN npm config set @freighttiger:registry http://nexus.tools.freighttiger.com/repository/ft-npm-group/
RUN apk add --no-cache g++ make bash python2 ca-certificates
RUN npm ci --only=production

FROM node:14-alpine
RUN apk add --no-cache curl bash ca-certificates \
    && addgroup -S ft && adduser -S ft -G ft
WORKDIR /opt/app
USER ft
COPY  --chown=ft:ft --from=build /opt/app/node_modules /opt/app/node_modules
COPY --chown=ft:ft ./app /opt/app

CMD ["node","index.js"]
EXPOSE 8080
